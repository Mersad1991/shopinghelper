package com.example.am.shopinghelper.product;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class MyDBHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "shopbaza.db";
    public static final String TABLE_PRODUCTS = "products";

    //name of columns
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_PRODUCTNAME = "name";
    public static final String COLUMN_NUMBER_OF_ITEMS = "number_of_items"; // Quantity
    public static final String COLUMN_DESC = "desc";
    public static final String COLUMN_PRICE = "price"; // Price of 1 item quantity
    public static final String COLUMN_PRICE_IN_DECIMAL = "price_in_decimal"; // Price of all items bought
    public static final String COLUMN_NUM_BEEN_BOUGHT = "num_been_bought"; // Quantity which was bought till now
    public static final String COLUMN_DATE_BOUGHT = "date_bought";

    public MyDBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String querry = "CREATE TABLE " + TABLE_PRODUCTS + "(" +
                COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_PRODUCTNAME + " TEXT, " +
                COLUMN_NUMBER_OF_ITEMS + " TEXT, " +
                COLUMN_DESC + " TEXT, " +
                COLUMN_PRICE + " TEXT, " +
                COLUMN_PRICE_IN_DECIMAL + " TEXT, " +
                COLUMN_NUM_BEEN_BOUGHT + " TEXT, " +
                COLUMN_DATE_BOUGHT + " TEXT " +
                ");";
        db.execSQL(querry);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PRODUCTS);
        onCreate(db);
    }

    public void addProduct() {
        ContentValues values = new ContentValues();
        GregorianCalendar cal = (GregorianCalendar) Calendar.getInstance();
        int day_of_month = cal.get(Calendar.DAY_OF_MONTH);
        int month = cal.get(Calendar.MONTH) + 1; // Jan. 0 - Dec. 11
        int year = cal.get(Calendar.YEAR);

        String day_of_month1 = String.format("%02d", day_of_month);
        String month1 = String.format("%02d", month);

        String date = year + "-" + month1 + "-" + day_of_month1;
        SQLiteDatabase db = getWritableDatabase();

        // In case i ever need to create dummy items... here will be the place
        for (ProductContent.ProductItem oneItem : ProductContent.ITEMS) {
            if (oneItem.num_been_bought > 0) {
                values.put(COLUMN_PRODUCTNAME, oneItem.name);
                values.put(COLUMN_NUMBER_OF_ITEMS, oneItem.number_of_items);
                values.put(COLUMN_DESC, oneItem.desc);
                values.put(COLUMN_PRICE, oneItem.price);
                values.put(COLUMN_PRICE_IN_DECIMAL, oneItem.price_in_decimal.toString());
                values.put(COLUMN_NUM_BEEN_BOUGHT, oneItem.num_been_bought);
                values.put(COLUMN_DATE_BOUGHT, date);

                db.insert(TABLE_PRODUCTS, null, values);
            }
        }

        db.close();
    }

    public String getAllfromDB() {
        String dbString = "";
        SQLiteDatabase db = getWritableDatabase();
        String querry = "SELECT * FROM " + TABLE_PRODUCTS + " WHERE 1";

        // Cursor point to location in your results
        Cursor c = db.rawQuery(querry, null);
        c.moveToFirst();

        while (!c.isAfterLast()) {
            if (c.getString(c.getColumnIndex(COLUMN_PRODUCTNAME)) != null) {
                dbString += c.getString(c.getColumnIndex(COLUMN_ID)) + " | ";
                dbString += "Name: " + c.getString(c.getColumnIndex(COLUMN_PRODUCTNAME)) + " | ";
                dbString += "Desc: " + c.getString(c.getColumnIndex(COLUMN_DESC)) + " | ";
                dbString += "Quantity bought: " + c.getString(c.getColumnIndex(COLUMN_NUM_BEEN_BOUGHT)) + " | ";
                dbString += "Price: " + c.getString(c.getColumnIndex(COLUMN_PRICE)) + " kn | ";
                dbString += "Total price: " + c.getString(c.getColumnIndex(COLUMN_PRICE_IN_DECIMAL)) + " kn | ";
                dbString += "Date: " + c.getString(c.getColumnIndex(COLUMN_DATE_BOUGHT)) + " | ";
                dbString += "\n --- \n";
            }
            c.moveToNext();
        }
        db.close();
        return dbString;
    }

    public String[] getAllNames() {
        ArrayList<String> dbNames = new ArrayList<>();

        SQLiteDatabase db = getWritableDatabase();
        String querry = "SELECT DISTINCT " + COLUMN_PRODUCTNAME + " FROM " + TABLE_PRODUCTS + " WHERE 1";

        Cursor c = db.rawQuery(querry, null);
        c.moveToFirst();

        while (!c.isAfterLast()) {
            if (c.getString(c.getColumnIndex(COLUMN_PRODUCTNAME)) != null) {
                dbNames.add(c.getString(c.getColumnIndex(COLUMN_PRODUCTNAME)));
            }
            c.moveToNext();
        }

        db.close();
        return dbNames.toArray(new String[dbNames.size()]);
    }

    public String searchForNames(String name) {
        String dbString = "";
        SQLiteDatabase db = getWritableDatabase();
        String querry = "SELECT * FROM " + TABLE_PRODUCTS + " WHERE " + COLUMN_PRODUCTNAME + "=\"" + name + "\";";

        // Cursor point to location in your results
        Cursor c = db.rawQuery(querry, null);
        c.moveToFirst();

        while (!c.isAfterLast()) {
            if (c.getString(c.getColumnIndex(COLUMN_PRODUCTNAME)) != null) {
                dbString += c.getString(c.getColumnIndex(COLUMN_ID)) + " | ";
                dbString += "Name: " + c.getString(c.getColumnIndex(COLUMN_PRODUCTNAME)) + " | ";
                dbString += "Desc: " + c.getString(c.getColumnIndex(COLUMN_DESC)) + " | ";
                dbString += "Quantity bought: " + c.getString(c.getColumnIndex(COLUMN_NUM_BEEN_BOUGHT)) + " | ";
                dbString += "Price: " + c.getString(c.getColumnIndex(COLUMN_PRICE)) + " kn | ";
                dbString += "Total price: " + c.getString(c.getColumnIndex(COLUMN_PRICE_IN_DECIMAL)) + " kn | ";
                dbString += "Date: " + c.getString(c.getColumnIndex(COLUMN_DATE_BOUGHT)) + " | ";
                dbString += "\n --- \n";
            }
            c.moveToNext();
        }
        db.close();
        return dbString;
    }

    public String[] getAllYears() {
        ArrayList<String> dbYears = new ArrayList<>();

        SQLiteDatabase db = getWritableDatabase();
        String querry = "SELECT DISTINCT strftime('%Y', " + COLUMN_DATE_BOUGHT + ") AS Year FROM " + TABLE_PRODUCTS + " WHERE 1";

        Cursor c = db.rawQuery(querry, null);
        c.moveToFirst();

        while (!c.isAfterLast()) {
            if (c.getString(c.getColumnIndex("Year")) != null) {
                dbYears.add(c.getString(c.getColumnIndex("Year")));
            }
            c.moveToNext();
        }

        db.close();
        return dbYears.toArray(new String[dbYears.size()]);
    }

    public ArrayList<ItemFromDatabase> getAllInMonth(String month, String year) {
        ArrayList<ItemFromDatabase> dbItems = new ArrayList<>();

        GregorianCalendar cal = (GregorianCalendar) Calendar.getInstance();
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.MONTH, (Integer.parseInt(month) - 1));
        int first_day = 1;
        int last_day;
        last_day = cal.getActualMaximum(Calendar.DATE);

        String tmp_last = String.format("%02d", last_day);
        String tmp_first = String.format("%02d", first_day);

        String begin_date = year + "-" + month + "-" + tmp_first;
        String last_date = year + "-" + month + "-" + tmp_last;

        SQLiteDatabase db = getWritableDatabase();
        String querry = "SELECT * FROM " + TABLE_PRODUCTS + " WHERE DATE(" + COLUMN_DATE_BOUGHT + ") BETWEEN \"" + begin_date + "\" AND \"" + last_date + "\";" ;

        Cursor c = db.rawQuery(querry, null);
        c.moveToFirst();

        while (!c.isAfterLast()) {
            if (c.getString(c.getColumnIndex(COLUMN_PRODUCTNAME)) != null) {
                String id = c.getString(c.getColumnIndex(COLUMN_ID));
                String name = c.getString(c.getColumnIndex(COLUMN_PRODUCTNAME));
                String number_of_items = c.getString(c.getColumnIndex(COLUMN_NUMBER_OF_ITEMS));
                String desc = c.getString(c.getColumnIndex(COLUMN_DESC));
                String price = c.getString(c.getColumnIndex(COLUMN_PRICE));
                String num_been_bought = c.getString(c.getColumnIndex(COLUMN_NUM_BEEN_BOUGHT));
                String price_dec = c.getString(c.getColumnIndex(COLUMN_PRICE_IN_DECIMAL));
                String date_from_item = c.getString(c.getColumnIndex(COLUMN_DATE_BOUGHT));

                dbItems.add(new ItemFromDatabase(id, name, number_of_items, desc, price, num_been_bought, price_dec, date_from_item));
            }
            c.moveToNext();
        }

        db.close();

        return dbItems;
    }

    public ArrayList<ItemFromDatabase> getAllToday(int time_frame) {
        ArrayList<ItemFromDatabase> dbItems = new ArrayList<>();

        GregorianCalendar cal = (GregorianCalendar) Calendar.getInstance();
        int day_of_month = cal.get(Calendar.DAY_OF_MONTH);
        int month = cal.get(Calendar.MONTH) + 1; // Jan. 0 - Dec. 11
        int year = cal.get(Calendar.YEAR);

        String today1 = String.format("%02d", day_of_month);
        String today2 = String.format("%02d", month);
        String today = year + "-" + today2 + "-" + today1;

        // 1- today, 2 - last 7 days, 3 - last 31 days
        if (time_frame == 2) {
            cal.add(Calendar.DATE, -7);
            day_of_month = cal.get(Calendar.DAY_OF_MONTH);
            month = cal.get(Calendar.MONTH) + 1;
            year = cal.get(Calendar.YEAR);
        } else if (time_frame == 3) {
            cal.add(Calendar.DATE, -31);
            day_of_month = cal.get(Calendar.DAY_OF_MONTH);
            month = cal.get(Calendar.MONTH) + 1;
            year = cal.get(Calendar.YEAR);
        }

        String day_of_month1 = String.format("%02d", day_of_month);
        String month1 = String.format("%02d", month);

        String date = year + "-" + month1 + "-" + day_of_month1;

        SQLiteDatabase db = getWritableDatabase();
        String querry = "";
        if (time_frame == 1) {
            querry = "SELECT * FROM " + TABLE_PRODUCTS + " WHERE " + COLUMN_DATE_BOUGHT + "=\"" + date + "\";";
        } else if (time_frame == 2 || time_frame == 3) {
            querry = "SELECT * FROM " + TABLE_PRODUCTS + " WHERE DATE(" + COLUMN_DATE_BOUGHT + ") BETWEEN \"" + date + "\" AND \"" + today + "\";" ;
        }

        Cursor c = db.rawQuery(querry, null);
        c.moveToFirst();

        while (!c.isAfterLast()) {
            if (c.getString(c.getColumnIndex(COLUMN_PRODUCTNAME)) != null) {
                String id = c.getString(c.getColumnIndex(COLUMN_ID));
                String name = c.getString(c.getColumnIndex(COLUMN_PRODUCTNAME));
                String number_of_items = c.getString(c.getColumnIndex(COLUMN_NUMBER_OF_ITEMS));
                String desc = c.getString(c.getColumnIndex(COLUMN_DESC));
                String price = c.getString(c.getColumnIndex(COLUMN_PRICE));
                String num_been_bought = c.getString(c.getColumnIndex(COLUMN_NUM_BEEN_BOUGHT));
                String price_dec = c.getString(c.getColumnIndex(COLUMN_PRICE_IN_DECIMAL));
                String date_from_item = c.getString(c.getColumnIndex(COLUMN_DATE_BOUGHT));

                dbItems.add(new ItemFromDatabase(id, name, number_of_items, desc, price, num_been_bought, price_dec, date_from_item));
            }
            c.moveToNext();
        }

        db.close();

        return dbItems;
    }

    public static class ItemFromDatabase {
        public String id, name, number_of_items, desc, price, num_been_bought, price_dec, date_from_item;

        public ItemFromDatabase(String id, String name, String number_of_items, String desc, String price, String num_been_bought, String price_dec, String date_from_item) {
            this.date_from_item = date_from_item;
            this.desc = desc;
            this.id = id;
            this.name = name;
            this.num_been_bought = num_been_bought;
            this.number_of_items = number_of_items;
            this.price = price;
            this.price_dec = price_dec;
        }
    }
}
