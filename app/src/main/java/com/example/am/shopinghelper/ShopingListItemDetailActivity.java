package com.example.am.shopinghelper;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

public class ShopingListItemDetailActivity extends AppCompatActivity implements ShopingListItemDetailFragment.RefreshSectionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopinglistitem_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.detail_toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        if (savedInstanceState == null) {
            creatingFragmentDetail();
        }
    }

    @Override
    public void creatingFragmentDetail() {
        Bundle arguments = new Bundle();
        arguments.putString(ShopingListItemDetailFragment.ARG_ITEM_ID,
                getIntent().getStringExtra(ShopingListItemDetailFragment.ARG_ITEM_ID));
        ShopingListItemDetailFragment fragment = new ShopingListItemDetailFragment();
        fragment.setArguments(arguments);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.shopinglistitem_detail_container, fragment)
                .commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            navigateUpTo(new Intent(this, ShopingListItemListActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
