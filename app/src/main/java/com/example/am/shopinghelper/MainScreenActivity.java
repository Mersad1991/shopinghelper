package com.example.am.shopinghelper;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class MainScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    public void onShopingListClick(View view) {
        Intent i = new Intent(this, ShopingListItemListActivity.class);
        startActivity(i);
    }

    public void onStatsClick(View view) {
        Intent i = new Intent(this, StatsActivity.class);
        startActivity(i);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            String msg = "Use \"Shoping List\" button to create a shoping list. New items are added via " +
                    "shoping list menu.\nWhen adding a new item, you can enter item name, quantity of that item that you are planing to buy, short item description, and price per item quantity.\nNote that fields have some character restrictions." +
                    "Name (50 chars.), Quantity (5 chars.), Item description (100 chars.), Price (8 chars. or 5 chars -> decimal point -> 2 chars).\nUse \"Purchases statistics\" to view " +
                    "stats for items that you bought today, in last 7 weeks, or last month (31. days), or to simply view all products that you bought.";

            new AlertDialog.Builder(this)
                    .setMessage(msg).setPositiveButton("OK", null).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
