package com.example.am.shopinghelper.product;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProductContent {
    public static final List<ProductItem> ITEMS = new ArrayList<ProductItem>();
    public static final Map<String, ProductItem> ITEM_MAP = new HashMap<String, ProductItem>();
    public static BigDecimal price_of_all_items_ProductContent = new BigDecimal(0.00).setScale(2, RoundingMode.CEILING);

    static {
        if (ITEMS.isEmpty()) {
            make_dummy_ProductItem();
        }
    }

    public static void make_and_add_ProductItem(String id, String name, String quantity, String desc, String price) {
        addItem(new ProductItem(id, name, quantity, desc, price));
    }

    public static void delete_item_with_id(String id) {
        Integer temp_id = Integer.parseInt(id);

        if (temp_id != 0 && ITEMS.size() == 1) {
            make_dummy_ProductItem();
        }

        if (temp_id == 0 && ITEMS.size() == 1) {
            return;
        }

        if (ITEM_MAP.containsKey(id)) {
            int i = ITEMS.indexOf(ITEM_MAP.get(id));
            deleteItem(id, i);
        }
    }

    private static void addItem(ProductItem productItem) {
        ITEMS.add(productItem);
        ITEM_MAP.put(productItem.id, productItem);
    }

    private static void deleteItem(String id, int location) {
        ITEM_MAP.remove(id);
        ITEMS.remove(location);
    }

    public static void delete_list() {
        ITEM_MAP.clear();
        ITEMS.clear();
        price_of_all_items_ProductContent = price_of_all_items_ProductContent.subtract(price_of_all_items_ProductContent);
        make_dummy_ProductItem();
    }

    private static void make_dummy_ProductItem() {
        make_and_add_ProductItem("0", "No items in the list!\nUse overflow menu to \nadd new items", "0", "This is where item description will be.", "0.00");
    }

    public static void up_the_total_price(BigDecimal p, int buy_del) {
        if (buy_del == 1) {
            price_of_all_items_ProductContent = price_of_all_items_ProductContent.add(p);
        } else if (buy_del == 2) {
            price_of_all_items_ProductContent = price_of_all_items_ProductContent.subtract(p);
        }
    }

    public static class ProductItem {
        public final String id;
        public final String name;
        public final int number_of_items;
        public final String desc;
        public String price;
        public BigDecimal price_in_decimal;
        public int num_been_bought = 0;

        public ProductItem(String id, String name, String number_of_items, String desc, String price) {
            this.id = id;
            this.name = name;
            this.number_of_items = Integer.parseInt(number_of_items);
            this.desc = desc;
            if (price.isEmpty()) price = "0";
            this.price_in_decimal = new BigDecimal(price);
            this.price_in_decimal = this.price_in_decimal.setScale(2, RoundingMode.CEILING);
            this.price = this.price_in_decimal.toString();
            this.price_in_decimal = new BigDecimal(0.00);
        }

        public void buy_1_of_this_item() {
            this.num_been_bought++;
            this.price_in_decimal = this.price_in_decimal.add(new BigDecimal(this.price));
        }

        public void update_price_of_item(BigDecimal bigDecimal_price_update) {
            ProductContent.price_of_all_items_ProductContent = ProductContent.price_of_all_items_ProductContent.subtract(this.price_in_decimal);
            this.price = bigDecimal_price_update.toString();
            this.price_in_decimal = bigDecimal_price_update.multiply(new BigDecimal(num_been_bought));
            ProductContent.price_of_all_items_ProductContent = ProductContent.price_of_all_items_ProductContent.add(this.price_in_decimal);
        }

        @Override
        public String toString() {
            return name;
        }
    }
}
