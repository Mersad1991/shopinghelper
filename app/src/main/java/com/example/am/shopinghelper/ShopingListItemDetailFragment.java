package com.example.am.shopinghelper;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.am.shopinghelper.product.ProductContent;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class ShopingListItemDetailFragment extends Fragment implements View.OnClickListener {

    public static final String ARG_ITEM_ID = "item_id";

    private ProductContent.ProductItem mItem;

    RefreshSectionListener refreshCommander;

    public EditText price_update;

    public boolean delete_signal = false;

    public ShopingListItemDetailFragment() {
    }

    @Override
    public void onClick(View v) {
        String tmp_price_update = price_update.getText().toString();

        if (tmp_price_update.isEmpty() || delete_signal || mItem.id.equals("0")) {
            return;
        }

        mItem.update_price_of_item(new BigDecimal(tmp_price_update).setScale(2, RoundingMode.CEILING));
        refreshCommander.creatingFragmentDetail();
    }

    public interface RefreshSectionListener {
        void creatingFragmentDetail();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            refreshCommander = (RefreshSectionListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString());
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG_ITEM_ID)) {
            mItem = ProductContent.ITEM_MAP.get(getArguments().getString(ARG_ITEM_ID));

            Activity activity = this.getActivity();
            CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
            if (appBarLayout != null) {
                appBarLayout.setTitle(mItem.name);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (mItem.id.equals("0")) {
            View nuView = inflater.inflate(R.layout.null_detail_fragment, container, false);
            return nuView;
        }

        View rootView = inflater.inflate(R.layout.detail_fragment, container, false);
        Button del = (Button) rootView.findViewById(R.id.delete_button);
        Button buy = (Button) rootView.findViewById(R.id.buy_button);
        Button update_price_button = (Button) rootView.findViewById(R.id.update_price_frag_button);
        delete_signal = false;

        if (mItem != null) {
            price_update = (EditText) rootView.findViewById(R.id.new_price_frag_editText);

            String tmp_name = "Name: " + mItem.name;
            ((TextView) rootView.findViewById(R.id.name_frag_textView)).setText(tmp_name);

            String tmp_quantity = "Quantity: " + mItem.number_of_items;
            ((TextView) rootView.findViewById(R.id.detail_frag_textView)).setText(tmp_quantity);

            String tmp_num_of_bought_items = "Currently you have: " + mItem.num_been_bought;
            TextView tmp_view_current_item_num = (TextView) rootView.findViewById(R.id.current_bought_frag_textView);
            tmp_view_current_item_num.setText(tmp_num_of_bought_items);

            String tmp_desc = "Item description: " + mItem.desc;
            ((TextView) rootView.findViewById(R.id.desc_frag_textView)).setText(tmp_desc);

            String tmp_price = "Price per quantity: " + mItem.price + " kn";
            ((TextView) rootView.findViewById(R.id.price_frag_textView)).setText(tmp_price);

            String tmp_total_price = "Current total price of this item: " + mItem.price_in_decimal + " kn";
            TextView tmp_view_current_total_price = (TextView) rootView.findViewById(R.id.currentTotalPrice_frag_textView);
            tmp_view_current_total_price.setText(tmp_total_price);

            if (mItem.number_of_items == mItem.num_been_bought) {
                ((TextView) rootView.findViewById(R.id.bought_flag_frag_textView)).setText("KUPLJENO!!!");
            }

            del.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ProductContent.delete_item_with_id(mItem.id);
                    ProductContent.up_the_total_price(mItem.price_in_decimal, 2);
                    delete_signal = true;
                    Toast.makeText(getContext(), "Item has been deleted successfully!", Toast.LENGTH_SHORT).show();
                }
            });

            buy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mItem.id.equals("0") || delete_signal) {
                        return;
                    }

                    if (mItem.num_been_bought == mItem.number_of_items) {
                        Toast.makeText(getContext(), "You bought every quantity of this item!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    mItem.buy_1_of_this_item();
                    ProductContent.up_the_total_price(new BigDecimal(mItem.price), 1);
                    Toast.makeText(getContext(), "BUY! - " + (mItem.num_been_bought) + " of this item is bought", Toast.LENGTH_SHORT).show();
                    refreshCommander.creatingFragmentDetail();
                }
            });

            update_price_button.setOnClickListener(this);
        }

        return rootView;
    }
}
