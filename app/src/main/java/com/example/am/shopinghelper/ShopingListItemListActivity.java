package com.example.am.shopinghelper;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.am.shopinghelper.product.FinishSaveDialogFragment;
import com.example.am.shopinghelper.product.ProductContent;

import java.math.BigDecimal;
import java.util.List;

public class ShopingListItemListActivity extends AppCompatActivity {
    public static boolean from_on_Create;
    public static BigDecimal price_of_all = ProductContent.price_of_all_items_ProductContent;
    public static FinishSaveDialogFragment dial_frag = new FinishSaveDialogFragment();

    private boolean mTwoPane;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopinglistitem_list);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());

        // Show the Up button in the action bar.
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        from_on_Create = true;
        refreshing_items_list();
    }

    private void refreshing_items_list() {
        price_of_all = ProductContent.price_of_all_items_ProductContent;
        TextView price_all = (TextView) findViewById(R.id.toolbar_price_counter_list);
        String tmp_priceall = price_of_all.toString() + " kn";
        price_all.setText(tmp_priceall);

        View recyclerView = findViewById(R.id.shopinglistitem_list);
        assert recyclerView != null;
        setupRecyclerView((RecyclerView) recyclerView);

        if (findViewById(R.id.shopinglistitem_detail_container) != null) {
            mTwoPane = true;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_list_item, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.add_new_shopingitem) {
            Intent i = new Intent(this, AddNewItemActivity.class);
            startActivity(i);
            return true;
        } else if (item.getItemId() == R.id.clear_shopinglist) {
            ProductContent.delete_list();
            refreshing_items_list();
            return true;
        } else if (item.getItemId() == R.id.save_shopinglist) {
            dial_frag.show(getSupportFragmentManager(), "FinSaveDialogFragment");
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupRecyclerView(@NonNull RecyclerView recyclerView) {
        recyclerView.setAdapter(new SimpleItemRecyclerViewAdapter(ProductContent.ITEMS));
    }

    public class SimpleItemRecyclerViewAdapter
            extends RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.ViewHolder> {

        private final List<ProductContent.ProductItem> mValues;

        public SimpleItemRecyclerViewAdapter(List<ProductContent.ProductItem> items) {
            mValues = items;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.shopinglistitem_list_content, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            holder.mItem = mValues.get(position);

            String tmp = mValues.get(position).num_been_bought + "x";
            if (mValues.get(position).id.equals("0")) {
                tmp = "";
            }
            holder.mIdView.setText(tmp);

            String tmp1 = mValues.get(position).name;
            if (tmp1.length() > 20 && !(mValues.get(position).id.equals("0"))) {
                tmp1 = tmp1.substring(0, 16) + "...";
            }
            holder.mContentView.setText(tmp1);

            if (mValues.get(position).num_been_bought == mValues.get(position).number_of_items && !(mValues.get(position).id.equals("0"))) {
                holder.mBoughtView.setText("KUPLJENO");
                holder.mBoughtView.setTextColor(Color.GRAY);
                holder.mIdView.setTextColor(Color.GRAY);
                holder.mContentView.setTextColor(Color.GRAY);
                holder.mPriceView.setTextColor(Color.GRAY);
            } else if (mValues.get(position).id.equals("0")) {
                holder.mBoughtView.setText("");
            } else {
                holder.mBoughtView.setText(" - ");
            }

            String tmp_price_text = "Price: " + mValues.get(position).price + "kn";
            if (mValues.get(position).id.equals("0")) {
                tmp_price_text = "";
            }
            holder.mPriceView.setText(tmp_price_text);

            holder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mTwoPane) {
                        Bundle arguments = new Bundle();
                        arguments.putString(ShopingListItemDetailFragment.ARG_ITEM_ID, holder.mItem.id);
                        ShopingListItemDetailFragment fragment = new ShopingListItemDetailFragment();
                        fragment.setArguments(arguments);
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.shopinglistitem_detail_container, fragment)
                                .commit();
                    } else {
                        Context context = v.getContext();
                        Intent intent = new Intent(context, ShopingListItemDetailActivity.class);
                        intent.putExtra(ShopingListItemDetailFragment.ARG_ITEM_ID, holder.mItem.id);

                        context.startActivity(intent);
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return mValues.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public final View mView;
            public final TextView mIdView;
            public final TextView mContentView;
            public final TextView mBoughtView;
            public final TextView mPriceView;
            public ProductContent.ProductItem mItem;

            public ViewHolder(View view) {
                super(view);
                mView = view;
                mIdView = (TextView) view.findViewById(R.id.id);
                mContentView = (TextView) view.findViewById(R.id.content);
                mBoughtView = (TextView) view.findViewById(R.id.bought_confirmation_text);
                mPriceView = (TextView) view.findViewById(R.id.price_text_in_list_for_item);
            }

            @Override
            public String toString() {
                return super.toString() + " '" + mContentView.getText() + "'";
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!from_on_Create) {
            refreshing_items_list();
        }
    }
}
