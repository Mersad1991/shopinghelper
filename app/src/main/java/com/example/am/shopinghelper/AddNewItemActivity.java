package com.example.am.shopinghelper;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Toast;

import com.example.am.shopinghelper.product.MyApplication;
import com.example.am.shopinghelper.product.MyDBHelper;
import com.example.am.shopinghelper.product.ProductContent;

import java.util.List;

public class AddNewItemActivity extends AppCompatActivity {
    AutoCompleteTextView item_name;
    EditText quantity;
    EditText description;
    EditText price;
    MyDBHelper tmp_helper_addItem;
    String[] testing_acomplete;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_item);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tmp_helper_addItem = ((MyApplication) getApplicationContext()).getDBHelper();
        getDbItemsName();

        item_name = (AutoCompleteTextView) findViewById(R.id.ProductName_editText);
        quantity = (EditText) findViewById(R.id.Quantity_editText);
        description = (EditText) findViewById(R.id.Desc_editText);
        price = (EditText) findViewById(R.id.Price_editText);

        ArrayAdapter<String> adpt_ac = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, testing_acomplete);
        item_name.setAdapter(adpt_ac);
    }

    private void getDbItemsName() {
        testing_acomplete = tmp_helper_addItem.getAllNames();
    }


    public void addNewOnClick(View v) {
        String name_temp = item_name.getText().toString();

        if (name_temp.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Name mandatory field!", Toast.LENGTH_SHORT).show();
            return;
        }

        Integer id_num = ProductContent.ITEMS.size();
        Integer tmp_int = 1;

        if (id_num == 1) {
            tmp_int = 0;
        }

        while (ProductContent.ITEM_MAP.containsKey(id_num.toString())) {
            id_num += 1;
        }

        String id = id_num.toString();

        if (quantity.getText().toString().isEmpty()) {
            quantity.setText("1");
        }


        ProductContent.make_and_add_ProductItem(id, item_name.getText().toString(), quantity.getText().toString(), description.getText().toString(), price.getText().toString());
        Toast.makeText(getApplicationContext(), "New item added successfully!", Toast.LENGTH_SHORT).show();

        if (tmp_int == 0) {
            ProductContent.delete_item_with_id(tmp_int.toString());
        }

        item_name.setText("");
        quantity.setText("");
        description.setText("");
        price.setText("");
    }

    @Override
    public void onBackPressed() {
        ShopingListItemListActivity.from_on_Create = false;
        super.onBackPressed();
    }
}
