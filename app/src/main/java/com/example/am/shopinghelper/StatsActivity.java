package com.example.am.shopinghelper;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

public class StatsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stats);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void onProductsClick(View view) {
        Intent i = new Intent(this, ProductsActivity.class);
        startActivity(i);
    }

    public void onTodayClick(View view) {
        Intent i = new Intent(this, TodayActivity.class);
        startActivity(i);
    }

    public void onWeekClick(View view) {
        Intent i = new Intent(this, WeekActivity.class);
        startActivity(i);
    }

    public void onMonthClick(View view) {
        Intent i = new Intent(this, MonthActivity.class);
        startActivity(i);
    }

}
