package com.example.am.shopinghelper;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

import com.example.am.shopinghelper.product.MyApplication;
import com.example.am.shopinghelper.product.MyDBHelper;

public class ProductsActivity extends AppCompatActivity {
    MyDBHelper db_help_products;
    TextView products_info_stats;
    AutoCompleteTextView search_product_name;
    String[] names;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        db_help_products = ((MyApplication) getApplicationContext()).getDBHelper();
        products_info_stats = (TextView) findViewById(R.id.products_info_stats_textView);
        search_product_name = (AutoCompleteTextView) findViewById(R.id.search_product_stat_editText);

        names = db_help_products.getAllNames();
        ArrayAdapter<String> adpt_ac = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, names);
        search_product_name.setAdapter(adpt_ac);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void onAllProductsClick(View view) {
        String tmp = db_help_products.getAllfromDB();

        if (tmp.isEmpty()) {
            tmp = "No items in the list!";
        }

        products_info_stats.setText(tmp);
    }

    public void onSearchProductClick(View view) {
        String tmp1 = search_product_name.getText().toString();

        if (tmp1.isEmpty()) {
            return;
        }

        String tmp = db_help_products.searchForNames(tmp1);
        products_info_stats.setText(tmp);
        search_product_name.setText("");
    }

}
