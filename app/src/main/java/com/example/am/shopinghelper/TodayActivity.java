package com.example.am.shopinghelper;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.example.am.shopinghelper.product.MyApplication;
import com.example.am.shopinghelper.product.MyDBHelper;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

public class TodayActivity extends AppCompatActivity {
    MyDBHelper db_help_today;
    TextView total_items_bought;
    TextView max_of_an_item;
    TextView min_of_an_item;
    TextView max_price;
    TextView min_price;
    TextView max_price_1;
    TextView min_price_1;
    TextView total_money_spent;
    TextView all_items_text;
    ArrayList<MyDBHelper.ItemFromDatabase> all_today_items = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_today);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        db_help_today = ((MyApplication) getApplicationContext()).getDBHelper();
        all_today_items = db_help_today.getAllToday(1);

        total_items_bought = (TextView) findViewById(R.id.total_items_bought_textView);
        max_of_an_item = (TextView) findViewById(R.id.max_of_an_item_textView);
        min_of_an_item= (TextView) findViewById(R.id.min_of_an_item_textView);
        max_price = (TextView) findViewById(R.id.max_price_textView);
        min_price = (TextView) findViewById(R.id.min_price_textView);
        max_price_1 = (TextView) findViewById(R.id.max_price_1_textView);
        min_price_1 = (TextView) findViewById(R.id.min_price_1_textView);
        total_money_spent = (TextView) findViewById(R.id.total_spent_textView);
        all_items_text = (TextView) findViewById(R.id.all_items_details_textView9);

        if (!all_today_items.isEmpty()) {
            fill_fields();
        } else {
            all_items_text.setText("No items for today!");
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void fill_fields() {
        String all_items_info = "";
        String tmp1 = all_today_items.size() + " items bought";
        total_items_bought.setText(tmp1);

        int maxQuantity = 0, minQuantity = Integer.MAX_VALUE;
        String maxQuantityName = "", minQuantityName = "", maxPriceName = "", minPriceName = "", maxPrice1Name = "", minPrice1Name = "";
        BigDecimal maxPrice = new BigDecimal(0.00).setScale(2, RoundingMode.CEILING), minPrice = new BigDecimal(Integer.MAX_VALUE).setScale(2, RoundingMode.CEILING);
        BigDecimal maxPrice1 = new BigDecimal(0.00).setScale(2, RoundingMode.CEILING), minPrice1 = new BigDecimal(Integer.MAX_VALUE).setScale(2, RoundingMode.CEILING);
        BigDecimal tmp_total_spent = new BigDecimal(0.00).setScale(2, RoundingMode.CEILING);

        for (MyDBHelper.ItemFromDatabase value : all_today_items) {
            all_items_info += value.id + " | ";
            all_items_info += "Name: " + value.name + " | ";
            all_items_info += "Desc: " + value.desc + " | ";
            all_items_info += "Quantity bought: " + value.num_been_bought + " | ";
            all_items_info += "Price: " + value.price + " kn | ";
            all_items_info += "Total price: " + value.price_dec + " kn | ";
            all_items_info += "Date: " + value.date_from_item + " | ";
            all_items_info += "\n --- \n";


            if (Integer.parseInt(value.num_been_bought) >= maxQuantity) {
                maxQuantity = Integer.parseInt(value.num_been_bought);
                maxQuantityName = value.name;
            }

            if (Integer.parseInt(value.num_been_bought) <= minQuantity) {
                minQuantity = Integer.parseInt(value.num_been_bought);
                minQuantityName = value.name;
            }

            if (!value.price.equals("0.00")) {
                BigDecimal tmpDec = new BigDecimal(value.price);
                int comp = tmpDec.compareTo(maxPrice);
                if (comp == 0 || comp == 1) {
                    maxPrice = new BigDecimal(tmpDec.toString());
                    maxPriceName = value.name;
                }

                int comp1 = tmpDec.compareTo(minPrice);
                if (comp1 == 0 || comp1 == -1) {
                    minPrice = new BigDecimal(tmpDec.toString());
                    minPriceName = value.name;
                }

                BigDecimal tmpPrice1 = new BigDecimal(value.price_dec);
                tmp_total_spent = tmp_total_spent.add(tmpPrice1);
                int comp2 = tmpPrice1.compareTo(maxPrice1);
                if (comp2 == 0 || comp2 == 1) {
                    maxPrice1 = new BigDecimal(tmpPrice1.toString());
                    maxPrice1Name = value.name;
                }

                int comp3 = tmpPrice1.compareTo(minPrice1);
                if (comp3 == 0 || comp3 == -1) {
                    minPrice1 = new BigDecimal(tmpPrice1.toString());
                    minPrice1Name = value.name;
                }
            }
        }

        String tmp_maxQuantityName = maxQuantityName, tmp_minQuantityName = minQuantityName, tmp_maxPriceName = maxPriceName,
                tmp_minPriceName = minPriceName, tmp_maxPrice1Name = maxPrice1Name, tmp_minPrice1Name = minPrice1Name;
        // find if there is more than one max, min, whatever
        for (MyDBHelper.ItemFromDatabase value : all_today_items) {
            if (maxQuantity == Integer.parseInt(value.num_been_bought)) {
                if (!maxQuantityName.equals(value.name)) {
                    tmp_maxQuantityName += ", " + value.name;
                }
            }

            if (minQuantity == Integer.parseInt(value.num_been_bought)) {
                if (!minQuantityName.equals(value.name)) {
                    tmp_minQuantityName += ", " + value.name;
                }
            }

            BigDecimal tmpMaxPrice = new BigDecimal(value.price);
            int compMaxPrice = tmpMaxPrice.compareTo(maxPrice);
            if (compMaxPrice == 0) {
                if (!maxPriceName.equals(value.name)) {
                    tmp_maxPriceName += ", " + value.name;
                }
            }

            // Dont mind the name... i just reused these variables, but this is for the minPrice
            compMaxPrice = tmpMaxPrice.compareTo(minPrice);
            if (compMaxPrice == 0) {
                if (!minPriceName.equals(value.name)) {
                    tmp_minPriceName += ", " + value.name;
                }
            }

            BigDecimal tmpMaxPrice1 = new BigDecimal(value.price_dec);
            int compMaxPrice1 = tmpMaxPrice1.compareTo(maxPrice1);
            if (compMaxPrice1 == 0) {
                if (!maxPrice1Name.equals(value.name)) {
                    tmp_maxPrice1Name += ", " + value.name;
                }
            }

            // Same as the above... just reusing stuff
            compMaxPrice1 = tmpMaxPrice1.compareTo(minPrice1);
            if (compMaxPrice1 == 0) {
                if (!minPrice1Name.equals(value.name)) {
                    tmp_minPrice1Name += ", " + value.name;
                }
            }
        }

        maxQuantityName = maxQuantity + " -> " + tmp_maxQuantityName;
        max_of_an_item.setText(maxQuantityName);
        minQuantityName = minQuantity + " -> " + tmp_minQuantityName;
        min_of_an_item.setText(minQuantityName);
        String max = maxPrice.toString() + " kn -> " + tmp_maxPriceName;
        max_price.setText(max);
        String min = minPrice.toString() + " kn -> " + tmp_minPriceName;
        min_price.setText(min);
        String maxP1 = maxPrice1.toString() + " kn -> " + tmp_maxPrice1Name;
        max_price_1.setText(maxP1);
        String minP1 = minPrice1.toString() + " kn -> " + tmp_minPrice1Name;
        min_price_1.setText(minP1);
        String tmp2_total_spent = tmp_total_spent.toString() + " kn";
        total_money_spent.setText(tmp2_total_spent);
        all_items_text.setText(all_items_info);
    }

}
