package com.example.am.shopinghelper;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.am.shopinghelper.product.MyApplication;
import com.example.am.shopinghelper.product.MyDBHelper;
import com.example.am.shopinghelper.product.ProductContent;

public class SaveFinishActivity extends AppCompatActivity {
    public static TextView t;
    ProductContent.ProductItem tmp_item;
    MyDBHelper tmp_dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_finish);

        tmp_dbHelper = ((MyApplication) getApplicationContext()).getDBHelper();

        t = (TextView) findViewById(R.id.fin_detail_textView);

        tmp_item = ProductContent.ITEMS.get(0);

        if (ProductContent.ITEMS.size() == 1 && tmp_item.id.equals("0")) {
            t.setText("No items in list!");
            return;
        }

        String tmpST = "";

        tmpST += "Total price: ";
        tmpST += ProductContent.price_of_all_items_ProductContent.toString() + " kn\n";

        tmpST += "Number of items: ";
        tmpST += ProductContent.ITEMS.size() + " items total\n";

        tmpST += "List of items:\n\t";
        for (ProductContent.ProductItem item : ProductContent.ITEMS) {
            tmpST += " - Name: ";
            tmpST += item.name;
            tmpST += "; ";
            tmpST += item.num_been_bought + "x ";
            tmpST += item.price + " kn";
            tmpST += "=" + item.price_in_decimal;
            if (item.number_of_items == item.num_been_bought) {
                tmpST += " - KUPLJENO";
            } else {
                tmpST += "; Quantity left to buy: " + (item.number_of_items - item.num_been_bought);
            }
            tmpST += "\n\t";
        }

        t.setText(tmpST);
    }

    public void onClickSaveFinish(View view) {
        if (ProductContent.ITEMS.size() == 1 && tmp_item.id.equals("0")) {
            Toast.makeText(getApplicationContext(), "No items! Nothing to save.", Toast.LENGTH_SHORT).show();
            return;
        }

        tmp_dbHelper.addProduct();
        Toast.makeText(getApplicationContext(), "Saved successfully!", Toast.LENGTH_SHORT).show();

        ProductContent.delete_list();
        ShopingListItemListActivity.from_on_Create = false;
    }
}
