package com.example.am.shopinghelper.product;

import android.app.Application;

public class MyApplication extends Application {
    private MyDBHelper dbHelper;

    public MyDBHelper getDBHelper() {
        return dbHelper;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        dbHelper = new MyDBHelper(this, null, null, 1);
    }

}
